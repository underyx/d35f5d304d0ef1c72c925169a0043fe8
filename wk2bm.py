#!/usr/bin/env python3
import requests
from os import environ

r = requests.get(
    'https://api.wanikani.com/v2/summary',
    headers={'Authorization': f'Bearer {environ["WANIKANI_TOKEN"]}'},
)
r.raise_for_status()

items_to_review = len(r.json()['data']['reviews'][0]['subject_ids'])
print(f'Submitting a value of {items_to_review} to Beeminder')

r = requests.post(
    'https://www.beeminder.com/api/v1/users/underyx/goals/process-wanikani-reviews/datapoints.json',
    data={'value': items_to_review},
    params={'auth_token': environ['BEEMINDER_TOKEN']},
)
r.raise_for_status()
